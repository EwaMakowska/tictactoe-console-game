import enums.PlayersType;

public class Player {

    private PlayersType type;
    private int points;

    public Player(PlayersType type, int points) {
        this.type = type;
        //TODO: points system
        this.points = points;
    }

    public PlayersType getType() {
        return type;
    }

    public void setType(PlayersType type) {
        this.type = type;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
