import enums.PlayersType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.temporal.ValueRange;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class TicTacToe {

    String[][] ticTacToeBoard = new String[3][3];
    Player player1;
    Player player2;
    Player currentPlayer;
    String field;


    public TicTacToe() {
        this.player1 = new Player(PlayersType.PLAYER_1, 0);
        this.player2 = new Player(PlayersType.PLAYER_2, 0);

        gameInitialize();

        getField();
    }

    private void createBoard() {
        for (int i = 0; i < ticTacToeBoard.length; i++) {
            for (int j = 0; j < ticTacToeBoard[i].length; j++) {
                ticTacToeBoard[i][j] = i + "" + j + " ";
                System.out.print(ticTacToeBoard[i][j]);
            }
            System.out.print("\n");
        }
    }

    private void gameInitialize() {
        createBoard();
        currentPlayer = player1;
    }

    private void play() {
        getField();
    }

    private void getField() {
        System.out.println("Gracz: " + currentPlayer.getType().getSymbol());
        System.out.println("Podaj numer komórki:");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            field = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(isIndexInRange(field) && isFieldFree(field)) {
            setField();
        } else {
            getField();
        }

    }

    private void setField() {
        int row = Integer.parseInt(String.valueOf(field.toCharArray()[0]));
        int col = Integer.parseInt(String.valueOf(field.toCharArray()[1]));

        ticTacToeBoard[row][col] = currentPlayer.getType().getSymbol() + "  ";
        displayBoard();
    }

    private void displayBoard() {
        for (int i = 0; i < ticTacToeBoard.length; i++) {
            for (int j = 0; j < ticTacToeBoard[i].length; j++) {
                System.out.print(ticTacToeBoard[i][j]);
            }
            System.out.print("\n");
        }

        setUser();
    }

    private boolean setUser() {
        if(ifWin()) {
            System.out.println("Player "+ currentPlayer.getType().getSymbol() + " wygrał \n KONIEC GRY!");
            return false;
            //TODO: play again
        }
        if (currentPlayer.getType() == PlayersType.PLAYER_1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }

        play();
        return true;
    }

    private boolean ifWin() {
        int row = Integer.parseInt(String.valueOf(field.toCharArray()[0]));
        int col = Integer.parseInt(String.valueOf(field.toCharArray()[1]));
        boolean result = false;

        int i = 0;
        int j = 0;
        while(i < ticTacToeBoard.length) {
                if(ticTacToeBoard[i][j].trim().equals(currentPlayer.getType().getSymbol()) &&
                    ticTacToeBoard[i][j+1].trim().equals(currentPlayer.getType().getSymbol()) &&
                    ticTacToeBoard[i][j+2].trim().equals(currentPlayer.getType().getSymbol())) {
                        result = true;
                }
                if(ticTacToeBoard[j][i].trim().equals(currentPlayer.getType().getSymbol()) &&
                    ticTacToeBoard[j+1][i].trim().equals(currentPlayer.getType().getSymbol()) &&
                    ticTacToeBoard[j+2][i].trim().equals(currentPlayer.getType().getSymbol())) {
                        result = true;
                }
                i++;
        }

        if(ticTacToeBoard[0][0].trim().equals(currentPlayer.getType().getSymbol()) &&
            ticTacToeBoard[1][1].trim().equals(currentPlayer.getType().getSymbol()) &&
            ticTacToeBoard[2][2].trim().equals(currentPlayer.getType().getSymbol())) {
                result = true;
        }
        if(ticTacToeBoard[0][2].trim().equals(currentPlayer.getType().getSymbol()) &&
                ticTacToeBoard[1][1].trim().equals(currentPlayer.getType().getSymbol()) &&
                ticTacToeBoard[2][0].trim().equals(currentPlayer.getType().getSymbol())) {
            result = true;
        }

        return result;
    }

    private boolean isFieldFree(String field) {
        if (ticTacToeBoard[getFieldRowAndCol(field).get("row")][getFieldRowAndCol(field).get("col")].trim().equals(PlayersType.PLAYER_1.getSymbol()) ||
             ticTacToeBoard[getFieldRowAndCol(field).get("row")][getFieldRowAndCol(field).get("col")].trim().equals(PlayersType.PLAYER_2.getSymbol())) {
            System.out.println("Pole zajęte.\n Spróbuj ponownie.");
            return false;
        }

        return true;
    }

    private boolean isIndexInRange(String field) {
        if(!ValueRange.of(0, 2).isValidIntValue(getFieldRowAndCol(field).get("row")) ||
            !ValueRange.of(0, 2).isValidIntValue(getFieldRowAndCol(field).get("col"))) {
            System.out.println("Numer kolumny i wiersza powinny być z zakresu 0 - 2.\n Spróbuj ponownie.");
            return false;
        }

        return true;
    }

    private Map<String, Integer> getFieldRowAndCol(String field) {
        Map<String, Integer> fieldIndex = new HashMap<>();

        int row = Integer.parseInt(String.valueOf(field.toCharArray()[0]));
        int col = Integer.parseInt(String.valueOf(field.toCharArray()[1]));

        fieldIndex.put("row", row);
        fieldIndex.put("col", col);

        return fieldIndex;
    }
}
