package enums;

public enum PlayersType {
    PLAYER_1("O"),
    PLAYER_2("X");

    private String symbol;

    PlayersType(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
